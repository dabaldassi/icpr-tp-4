#include <ros/ros.h>
#include <std_msgs/Float64.h>

int main(int argc, char *argv[])
{
  constexpr float PI = 3.141592635;
  constexpr int   RATE = 1000; // Rate in Hz
  
  ros::init(argc,argv,"lidar_rotation");
  ros::NodeHandle nh;
  ros::Publisher pub = nh.advertise<std_msgs::Float64>("laser_velocity_controller/command",RATE);
  ros::Rate rate(RATE);

  while(ros::ok()) {
    std_msgs::Float64 msg;
    msg.data = PI/14;
    pub.publish(msg);
    rate.sleep();
  }
  
  return 0;
}

