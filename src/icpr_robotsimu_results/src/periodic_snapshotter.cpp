#include <ros/ros.h>
#include <laser_assembler/AssembleScans.h>
#include <sensor_msgs/PointCloud.h>

int main(int argc, char * argv[])
{
	using namespace laser_assembler;
	constexpr int RATE = 1;
	
	ros::init(argc,argv,"periodic_snapshotter");
	ros::NodeHandle nh;
  	ros::Publisher pub = nh.advertise<sensor_msgs::PointCloud>("/cloud",RATE);
  	ros::Rate rate(RATE);
  	ros::service::waitForService("assemble_scans");
    ros::ServiceClient client = nh.serviceClient<AssembleScans>("assemble_scans");
 
 	AssembleScans srv;
    srv.request.begin = ros::Time(0,0);
    srv.request.end   = ros::Time::now();
  	
  	while(ros::ok()) {
  		sensor_msgs::PointCloud msg;
 	    if (client.call(srv)) {
 	        // printf("Got cloud with %u points\n", srv.response.cloud.points.size());
 	        msg = srv.response.cloud;
 	        pub.publish(msg);
 	    }
      	else {
      	   printf("Service call failed\n");
      	}
      	
  	  	rate.sleep();
  	}
}
